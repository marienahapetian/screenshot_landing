<?php
$title = 'Thank You';
require_once __DIR__ . '/../autoload.php';

$auto_dl = true;
$page = 'download';
?>
<?php include_once __DIR__ . '/../inc/header.php'; ?>

    <div id="content" class="download-page clearfix">
        <section id='fun-facts' class="section-spacing">
            <div class="banner-caption">
                <div class="container">
                    <h3>
                        Thank you for downloading Screenley for Windows!<br>
                        Install it on your computer and use it to be more productive while working remotely.
                    </h3>
                    <div id="hotkeys">
                        <ul class="flex flex-horizontal-center">
                            <li>
                                <h4 >Hotkeys:</h4>
                            </li>
                            <li>
                                PrtScn - Capture the screen
                            </li>
                            <li>
                                Ctrl+D - Upload to scrly.co server to share the short link
                            </li>
                            <li>
                                Ctrl+S - Save on desktop
                            </li>
                            <li>
                                Ctrl+C - Copy to clipboard
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!-- end thankyou -->

        <?php include_once __DIR__ . '/../inc/contact-form.php'; ?>

    </div>
<?php include_once __DIR__ . '/../inc/footer.php'; ?>