<?php $page = 'about';?>

<?php include_once __DIR__ . '/../inc//header.php'; ?>

    <div id="content"  class="clearfix">
        <section id="faq" class="no-spacing">
            <div class="circle1"></div>
            <div class="circle2"></div>
            <div class="circle3"></div>
            <div class="circle4"></div>
            <div class="banner-caption">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="heading text-white">About Us</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end home -->


        <section id="features" class="spacing-70">
            <div class="container">
                <div class="">
                    <p>
                        After years of using some different screen capture desktop applications we realized that it is near impossible to find an app which is both free and at the same time has an option to upload and share short links, as well as has all the customization tools for daily usage. Most of the existing softwares are either paid or don't have customization tools or have limited storage or are full of ads.
                    </p>
                    <p>
                        We created Screenley with the user's perspective in mind. We wanted to create a simple and powerful tool which would be simple, have no ads on the web page, have nice design, smooth user experience and all the customization tools that freelancers and IT teams might need to make their screenshots speak for them. We keep it simple, so users can focus on their stuff and not on figuring out how to use the app. Best of all - it's free. You can install Screenley and start screen capturing and sending out the short links right away.
                    </p>
                    <p>
                        We would love to hear your suggestions for improvements or more features!
                    </p>
                </div>
            </div>
        </section>
        <!-- end features -->

    </div>

<?php include_once __DIR__ . '/../inc/footer.php'; ?>