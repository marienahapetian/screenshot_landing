<?php $page = 'cookies';?>

<?php include_once __DIR__ . '/../inc/header.php'; ?>

    <div id="content"  class="clearfix">
        <section id="faq" class="no-spacing">
            <div class="circle1"></div>
            <div class="circle2"></div>
            <div class="circle3"></div>
            <div class="circle4"></div>
            <div class="banner-caption">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="heading text-white">Cookie Policy</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end home -->


        <section id="features" class="spacing-70">
            <div class="container">
                <div class="sub-section">
                    <div class="sub-section-title">What are cookies?</div>
                    <p>
                        Cookies are small pieces of data, stored in text files, that are stored on your computer or other device when websites are loaded in a browser. They are widely used to “remember” you and your preferences, either for a single visit (through a “session cookie”) or for multiple repeat visits (using a “persistent cookie”). They ensure a consistent and efficient experience for visitors, and perform essential functions. Cookies may be set by the site that you are visiting (known as “first party cookies”), or by third parties, such as those who serve content or provide analytics services on the website (“third party cookies”).
                    </p>
                </div>
                <div class="sub-section">
                    <div class="sub-section-title">How do we use cookies?</div>
                    <p>
                        We use cookies for a number of different purposes. Some cookies are necessary for technical reasons; some enable a personalized experience for visitors, etc. Some of these cookies may be set when a page is loaded, or when a visitor takes a particular action.
                    </p>
                </div>
            </div>
        </section>
        <!-- end features -->

    </div>

<?php include_once __DIR__ . '/../inc/footer.php'; ?>