<?php $page = 'contact';?>

<?php include_once __DIR__ . '/../inc/header.php'; ?>

    <div id="content"  class="clearfix">
        <section id="faq" class="no-spacing">
            <div class="circle1"></div>
            <div class="circle2"></div>
            <div class="circle3"></div>
            <div class="circle4"></div>
            <div class="banner-caption">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="heading text-white">Contact Us</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end home -->

        <?php include_once __DIR__ . '/../inc/contact-form.php'; ?>

    </div>

<?php include_once __DIR__ . '/../inc/footer.php'; ?>