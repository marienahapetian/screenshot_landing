<?php $page = 'faq';?>

<?php include_once __DIR__ . '/../inc/header.php'; ?>

    <div id="content"  class="clearfix">
        <section id="faq" class="no-spacing">
            <div class="circle1"></div>
            <div class="circle2"></div>
            <div class="circle3"></div>
            <div class="circle4"></div>
            <div class="banner-caption">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                                <div class="heading text-white">Hello. How we can help you?</div>
                                <input type="text" class="search" placeholder="Search for questions">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end home -->

        <section id="features" class="spacing-70">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="text-center">
                            <div class="heading">Frequently Asked Questions</div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="faq-block">
                            <h3>General</h3>
                            <div class="accordion">
                                <h3>What is Screenley, and what are its main features</h3>
                                <div>
                                    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                                    <p><a class="more" href="/faq/what-is-screenley">Read More..</a></p>
                                </div>

                                <h3>Downloading and installing Screenley</h3>
                                <div>
                                    Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.
                                </div>
                            </div>
                        </div>

                        <div class="faq-block">
                            <h3>Image Sharing</h3>
                            <div class="accordion">
                                <h3>How can I share screenley images?</h3>
                                <div>
                                    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                                </div>

                                <h3>For how long are my screenshots not deleted?</h3>
                                <div>
                                    Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.
                                </div>

                                <h3>Is there an uploads limit?</h3>
                                <div>
                                    Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In suscipit faucibus urna.
                                </div>
                            </div>
                        </div>

                        <div class="faq-block">
                            <h3>Privacy</h3>
                            <div class="accordion">
                                <h3>Where can I read Screenley legal information?</h3>
                                <div>
                                    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                                </div>

                                <h3>Which images can be deleted by Screenley Team?</h3>
                                <div>
                                    Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In suscipit faucibus urna.
                                </div>

                                <h3>What data does Screenley save with each screenshot?</h3>
                                <div>
                                    Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="faq-block">
                            <h3>Files</h3>
                            <div class="accordion">
                                <h3>How to capture and share your screen in seconds?</h3>
                                <div>
                                    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                                </div>

                                <h3>How can I see screenshots & images I have uploaded a long time ago?</h3>
                                <div>
                                    Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In suscipit faucibus urna.
                                </div>

                                <h3>How can I draw & add notes on my images?</h3>
                                <div>
                                    Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.
                                </div>

                                <h3>How can I delete an image I uploaded?</h3>
                                <div>
                                    Nam enim risus, molestie et, porta ac, aliquam ac, risus. Quisque lobortis. Phasellus pellentesque purus in massa. Aenean in pede. Phasellus ac libero ac tellus pellentesque semper. Sed ac felis. Sed commodo, magna quis lacinia ornare, quam ante aliquam nisi, eu iaculis leo purus venenatis dui.
                                </div>
                            </div>
                        </div>

                        <div class="faq-block">
                            <h3>Support</h3>
                            <div class="accordion">
                                <h3>Is there an uploads limit?</h3>
                                <div>
                                    Sed non urna. Donec et ante. Phasellus eu ligula. Vestibulum sit amet purus. Vivamus hendrerit, dolor at aliquet laoreet, mauris turpis porttitor velit, faucibus interdum tellus libero ac justo. Vivamus non quam. In suscipit faucibus urna.
                                </div>

                                <h3>Support Service</h3>
                                <div>
                                    Mauris mauris ante, blandit et, ultrices a, suscipit eget, quam. Integer ut neque. Vivamus nisi metus, molestie vel, gravida in, condimentum sit amet, nunc. Nam a nibh. Donec suscipit eros. Nam mi. Proin viverra leo ut odio. Curabitur malesuada. Vestibulum a velit eu ante scelerisque vulputate.
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <!-- end features -->

    </div>

<?php include_once __DIR__ . '/../inc/footer.php'; ?>