<?php $page = 'faq-single';?>

<?php include_once __DIR__ . '/../inc/header.php'; ?>

    <div id="content"  class="clearfix">
        <section id="faq" class="no-spacing">
            <div class="circle1"></div>
            <div class="circle2"></div>
            <div class="circle3"></div>
            <div class="circle4"></div>
            <div class="banner-caption">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="heading text-white">What is Screenley?</div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end home -->


        <section id="features" class="spacing-70">
            <div class="container">
                <div class="breadcrumb">
                    <div class="breadcrumb-item"><a href="/faq">FAQ</a></div>
                    <div class="breadcrumb-item">What Is Screenley?</div>
                </div>
                <div class="faq-answer">
                    <p>
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                    </p>
                    <p>
                        Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?
                    </p>
                    <p>
                        Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
                    </p>
                </div>
            </div>
        </section>
        <!-- end features -->

    </div>

<?php include_once __DIR__ . '/../inc/footer.php'; ?>