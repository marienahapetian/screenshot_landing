<?php
class DBConnection extends DBConfig    {

    public $connectionString;
    public $dataSet;
    private $sqlQuery;

    protected $dbName;
    protected $hostName;
    protected $userName;
    protected $passCode;

    function Mysql()    {
        $this -> connectionString = NULL;
        $this -> sqlQuery = NULL;
        $this -> dataSet = NULL;

        $dbPara = new Dbconfig();
        $this -> dbName = $dbPara -> dbName;
        $this -> hostName = $dbPara -> serverName;
        $this -> userName = $dbPara -> userName;
        $this -> passCode = $dbPara ->passCode;
        $dbPara = NULL;
    }

    function dbConnect()    {
        $this -> connectionString = mysqli_connect($this -> serverName,$this -> userName,$this -> passCode);
        mysqli_select_db($this -> connectionString,$this -> dbName);
        return $this -> connectionString;
    }

    function dbDisconnect() {
        $this -> connectionString = NULL;
        $this -> sqlQuery = NULL;
        $this -> dataSet = NULL;
        $this -> dbName = NULL;
        $this -> hostName = NULL;
        $this -> userName = NULL;
        $this -> passCode = NULL;
    }

    function selectAll($tableName)  {
        $this -> sqlQuery = 'SELECT * FROM '.$this -> dbName.'.'.$tableName;
        $this -> dataSet = mysqli_query($this -> connectionString,$this -> sqlQuery);
        return $this -> dataSet;
    }

    function selectWhere( $tableName, $rowName, $operator, $value, $valueType )   {
        $this -> sqlQuery = 'SELECT * FROM '.$tableName.' WHERE '.$rowName.' '.$operator.' ';

        if($valueType == 'int') {
            $this -> sqlQuery .= $value;
        } else if($valueType == 'char')   {
            $value = mysqli_real_escape_string($this->connectionString,$value);
            $this -> sqlQuery .= "'".$value."'";
        }
        $this -> dataSet = mysqli_query($this -> connectionString,$this -> sqlQuery);
        $this -> sqlQuery = NULL;
        return $this -> dataSet;
        #return $this -> sqlQuery;
    }

    function insertInto( $tableName, array $keys, array $values) {
        if($keys) $this -> sqlQuery = "INSERT INTO ".$tableName." (".implode($keys,',').") VALUES ('";
        else $this -> sqlQuery = "INSERT INTO ".$tableName." VALUES ('";

        foreach($values as $key=>$valuesArray){
            $this->sqlQuery .= implode($valuesArray,"','") . "')";
        }

        mysqli_query($this ->connectionString,$this -> sqlQuery);
        return $this -> sqlQuery;
    }

    function selectFreeRun($query)  {
        $this -> dataSet = mysqli_query($this -> connectionString,$query);
        return $this -> dataSet;
    }

    function freeRun($query)    {
        return mysqli_query($this -> connectionString,$query);
    }
}
?>