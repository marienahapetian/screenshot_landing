<?php $page = 'index';?>

<?php include_once __DIR__ . '/inc/header.php'; ?>

    <div id="content"  class="clearfix">
        <section id="home" class="no-spacing">
            <div class="circle1"></div>
            <div class="circle2"></div>
            <div class="circle3"></div>
            <div class="circle4"></div>
            <div class="banner-caption">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="hero-text">
                                <h1 class="animated fadeInLeft col-md-8 no-padd clearfix">The simplest screen capture
                                    tool for Windows!</h1>

                                <p class="animated fadeInLeft">
                                    Screenley is a simple, powerful and completely FREE screen capturing tool with the
                                    ability to draw arrows, add text, box and more on images. It helps freelancers and
                                    IT teams to spend less time on explaining details while working remotely!
                                    <br><br>
                                    No watermarks, no monthly fees, unlimited free storage on our secure servers when
                                    using Screenley short links to share screenshots with your co-workers and clients.
                                </p>
                                <a href="/download" class="btn btn-primary animated fadeInLeft download-app"><i
                                        class="fa fa-download" ></i>Download</a>
                                <a href="#contact" class="btn btn-default animated fadeInLeft">Contact us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end home -->

        <section id="features" class="section-spacing">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="section-title text-center">
                            <div class="heading">Main Features</div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="features-item text-center wow fadeIn" data-wow-delay="0.2s">
                            <div class="icon">
                                <i class="fas fa-gem"></i>
                            </div>
                            <h3>Completely free</h3>

                            <p>
                                Screenley is a FREE tool which makes your work process more productive.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="features-item text-center wow fadeIn" data-wow-delay="0.3s">
                            <div class="icon">
                                <i class="fa fa-eye"></i>
                            </div>
                            <h3>Fast and Simple</h3>

                            <p>
                                We know the value of time. We designed Screenley keeping this in mind.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="features-item text-center wow fadeIn" data-wow-delay="0.5s">
                            <div class="icon">
                                <i class="fa fa-user"></i>
                            </div>
                            <h3>User friendly application</h3>

                            <p>Simple and intuitive UI/UX makes your work with the app easier and faster.</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="features-item text-center wow fadeIn" data-wow-delay="0.4s">
                            <div class="icon">
                                <i class="fas fa-sync-alt"></i>
                            </div>
                            <h3>Easy screenshot of selected area</h3>

                            <p>
                                Screenley allows you to select any area on your desktop and take a screenshot with two
                                clicks.
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="features-item text-center wow fadeIn" data-wow-delay="0.6s">
                            <div class="icon">
                                <i class="fa fa-link"></i>
                            </div>
                            <h3>Simply share the short link</h3>

                            <p>Upload your screenshot to our cloud server and get its short link with just one
                                click.</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="features-item text-center wow fadeIn" data-wow-delay="0.7s">
                            <div class="icon">
                                <i class="fas fa-edit"></i>
                            </div>
                            <h3>Online editors</h3>

                            <p>You can edit screenshots instantly when taking them using our easy-to-use and powerful
                                online editor.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end features -->

        <?php include_once __DIR__ . '/inc/contact-form.php'; ?>

    </div>

<?php include_once __DIR__ . '/inc/footer.php'; ?>