<section id="contact" class="<?php echo (isset($page) && $page!=='contact')?'section-spacing-bottom':'section-spacing-outer';?> white-bg">
    <div class="container">
        <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-center <?php echo $page=='download'?'spacing-50':'';?>">
                        <div class="heading">Got questions?</div>

                        <p>Don't hesitate to send us a message. We would love to hear back from you.</p>
                    </div>
                </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div class="contact-thumb text-md-left text-center">
                    <img src="img/contact/1.png" alt="contact">
                </div>
            </div>
            <div class="col-md-7">
                <form id="contactForm" class="contact-form wow zoomIn" action=""
                      data-toggle="validator" method="post" novalidate="true">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input placeholder="First Name" id="fname" class="form-control" name="fname"
                                       type="text" required="" data-error="Please enter your first name">

                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input placeholder="Last Name" id="lname" class="form-control" name="lname"
                                       type="text">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <input placeholder="Email Address" id="email" class="form-control" name="email"
                               type="email" required="" data-error="Please enter your valid email address">

                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                                <textarea placeholder="Your Comments" id="message" cols="20" rows="5"
                                          class="form-control" required=""
                                          data-error="Please enter your comments"></textarea>

                        <div class="help-block with-errors"></div>
                    </div>
                    <input value="Send Message" name="submit" class="btn btn-primary disabled" type="submit">

                    <div id="msgSubmit" class="h3 text-center hidden"></div>
                </form>
            </div>
        </div>

    </div>
</section>
<!-- end contact us -->