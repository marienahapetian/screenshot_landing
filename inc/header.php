<?php header('Cache-Control: max-age=31536000');?>
<?php require_once __DIR__ . '/../autoload.php';?>
<!DOCTYPE html>
<html lang="en">
<head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-132829694-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-132829694-1');
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Screenley, screenley@gmail.com">
    <meta name="description" content="Screenley is a simple, powerful and free screen capture tool with the ability to draw arrows, add text, box and more on screenshots and share short link.">
    <meta name="language" content="EN">
    <meta http-equiv="Cache-control" content="public">

    <?php if(isset($page) && $page=='index'){ ?>
        <link rel="canonical" href="https://screenley.com" />
    <?php } ?>

    <?php
    if(isset($auto_dl) && $auto_dl){ ?>
        <meta http-equiv="refresh" content="1;url=<?php echo $config['download_url'];?>" />
    <?php }
    ?>

    <title>Screenley | Screen Capture Tool for Freelancers and IT Teams</title>

    <link rel="shortcut icon" href="/img/favicon.png" type="image/x-icon">
    <!-- Bootstrap Core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <!-- Animate CSS -->
    <link href="/css/animate.css" rel="stylesheet">
    <!-- Font-awesome -->

    <link rel="stylesheet" href="/css/font-awesome.all.min.css">

    <?php if(isset($page) && $page=='faq'){ ?>
        <link rel="stylesheet" href="../css/accordion.css">
    <?php } ?>

    <!-- Main CSS -->
    <link href="/style.css" rel="stylesheet">
</head>

<body data-spy="scroll" data-target="#main-nav" data-offset="101" class="clearfix">
<!--Preload-->



<nav class="navbar navbar-expand-lg bg-white">
    <div class="container">
        <a class="navbar-brand" href="/">
            <img src="/img/logo.png" alt="Logo" height="70">
        </a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#main-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="main-nav">
            <ul class="navbar-nav ml-auto" id="nav">
                <li class="nav-item"><a class="nav-link <?php echo (isset($page) && $page=='index')?'active':'';?>" href="/">Home</a></li>
                <li class="nav-item"><a class="nav-link <?php echo (isset($page) && $page=='about')?'active':'';?>" href="/about-us">About Us</a></li>
<!--                <li class="nav-item"><a class="nav-link --><?php //echo (isset($page) && $page=='faq')?'active':'';?><!--" href="/faq">FAQ</a></li>-->
                <li class="nav-item"><a class="nav-link <?php echo (isset($page) && $page=='contact')?'active':'';?>" href="/contact-us">Contact Us</a></li>
                <li class="nav-item btn-download">
                    <a class="nav-link download-app" href="/download">
                        <i class="fab fa-windows"></i>Download free Windows APP
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- end nav -->