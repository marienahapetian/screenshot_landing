<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="copyright-text text-center">
                    <div>
                        <span> Screenley © <?php echo date('Y');?></span>
                        <ul class="social-icons text-center">
                            <li><a href="https://www.facebook.com/Screenley-280001576039129/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                        </ul>
                    </div>
                    <div>
                        <ul class="footer-menu text-center">
                            <li class="nav-item"><a class="nav-link" href="/cookies">Cookie Policy</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Bact to top -->
<div class="back-top">
    <a href="#"><i class="fa fa-angle-up"></i></a>
</div>

<!-- jQuery -->
<script src="/js/jquery-3.3.1.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.easing.min.js"></script>
<script src="/js/wow.js"></script>
<script src="/js/form-validator.min.js"></script>

<?php if(isset($page) && $page=='faq'){ ?>
    <script src="../js/jquery-ui.min.js"></script>
<?php } ?>

<script src="/js/main.js"></script>


</body></html>