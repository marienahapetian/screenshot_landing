(function($)
{
    "use strict";

    // Preloader
    jQuery(window).on('load', function() {
        preloader();
    });

    var headerHeight = jQuery('.navbar').outerHeight();
    jQuery('.navbar-nav li a').on('click', function(event) {
        jQuery('.navbar-nav li').removeClass('active');
        jQuery(this).parent().addClass('active');
        var $anchor = jQuery(this);

        jQuery('html, body').stop().animate({
            scrollTop: jQuery($anchor.attr('href')).offset().top-headerHeight
        }, 1000, 'easeInOutExpo');
        event.preventDefault();
    });

    jQuery('#content a[href*="#"]').on('click', function(event) {
        var $anchor = jQuery(this);

        jQuery('html, body').stop().animate({
            scrollTop: jQuery($anchor.attr('href')).offset().top
        }, 1000, 'easeInOutExpo');
        event.preventDefault();
    });


    jQuery(".navbar-nav li a").on("click",function(event){
        jQuery(".navbar-collapse").removeClass('show');
        jQuery('.navbar-toggler').addClass('collapsed');
    });

    // Animation section
    if(jQuery('.wow').length){
        var wow = new WOW(
            {
                boxClass:     'wow',      // animated element css class (default is wow)
                animateClass: 'animated', // animation css class (default is animated)
                offset:       0,          // distance to the element when triggering the animation (default is 0)
                mobile:       true,       // trigger animations on mobile devices (default is true)
                live:         true       // act on asynchronously loaded content (default is true)
            }
        );
        wow.init();
    }

    // CounterUp
    if(jQuery('.counter').length){
        jQuery('.counter').counterUp({
            delay: 10,
            time: 2000
        });
    }


    // App Video
    function autoPlayYouTubeModal() {
        var trigger = $("body").find('[data-toggle="modal"]');
        trigger.on("click",function() {
            var theModal = $(this).data("target"),
                videoSRC = $('#video-modal iframe').attr('src'),
                videoSRCauto = videoSRC + "?autoplay=1";
            $(theModal + ' iframe').attr('src', videoSRCauto);
            $(theModal + ' button.close').on("click",function() {
                $(theModal + ' iframe').attr('src', videoSRC);
            });
            $('.modal').on("click",function() {
                $(theModal + ' iframe').attr('src', videoSRC);
            });
        });
    }
    autoPlayYouTubeModal();

    // Back to top
    jQuery('.back-top a').on('click', function(event) {
        jQuery('body,html').animate({scrollTop:0},800);
        return false;
    });

    jQuery(window).on('scroll', function() {

        // Back to top
        if(jQuery(this).scrollTop()>150){
            jQuery('.back-top').fadeIn();
        }
        else{
            jQuery('.back-top').fadeOut();
        }
    });

    // Preload
    function preloader(){
        jQuery(".preloaderimg").fadeOut();
        jQuery(".preloader").delay(200).fadeOut("slow").delay(200, function(){
            jQuery(this).remove();
        });
    }

    // Vertical Center Modal
    function centerModals($element) {
        var $modals;
        if ($element.length) {
            $modals = $element;
        } else {
            $modals = jQuery('.modal-vcenter:visible');
        }
        $modals.each( function(i) {
            var $clone = jQuery(this).clone().css('display', 'block').appendTo('body');
            var top = Math.round(($clone.height() - $clone.find('.modal-content').height()) / 2);
            top = top > 0 ? top : 0;
            $clone.remove();
            jQuery(this).find('.modal-content').css("margin-top", top);
        });
    }

    jQuery('.modal-vcenter').on('show.bs.modal', function(e) {
        centerModals($(this));
    });
    jQuery(window).on('resize', centerModals);

})(jQuery);
//scriptjs

$("#contactForm").validator().on("submit", function (event) {
    if (event.isDefaultPrevented()) {
        formError();
    } else {
        event.preventDefault();
        submitForm();
    }
});


function submitForm(){
    // Initiate Variables With Form Content
    var fname = $("#fname").val();
    var lname = $("#lname").val();
    var email = $("#email").val();
    var message = $("#message").val();

    $('.with-errors').html('');

    $.ajax({
        type: "POST",
        url: "contact.php",
        data: "fname=" + fname + "&lname=" + lname + "&email=" + email + "&message=" + message,
        dataType: 'json',
        success : function(data){
            if (data.success){
                formSuccess();
            } else {
                formError();
                submitMSG(false, data.errors);
            }
        }
    });
}

function formSuccess(){
    $("#contactForm")[0].reset();
    submitMSG(true, "Thank you! Your email has been sent successfully.")
}

function formError(data){
    $("#contactForm").removeClass().addClass('animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $(this).removeClass();

    });
}
function submitMSG(valid, msgs){
    if(valid){
        var msgClasses = "text-success";

    } else {
        var msgClasses = "text-danger";
    }

    for (var key in msgs) {
        if (msgs.hasOwnProperty(key)) {
            var error = "<ul>";
            if($.isArray(msgs[key])){
                $.each(msgs[key], function( index, value ) {
                    error += '<li>'+value+'</li>';
                });
            } else {
                error += '<li>'+msgs[key]+'</li>';
            }

            error += "</ul>";

            $('#'+key).closest('.form-group').addClass('has-error').find('.with-errors').html(error);
        }
    }

    if(!$.isArray(msgs))  $("#msgSubmit").removeClass().addClass(msgClasses).text(msgs);
}
//contact form js

jQuery(document).ready(function(){
    if( $('body').outerHeight() < $(window).height() ){
        var diff = $(window).height() - $('body').outerHeight();
        var curr_height = $('#content').outerHeight();
        $('#content').height(curr_height + diff);
    }

    if($('.accordion').length){
        $( ".accordion").eq(0).accordion({
            collapsible: true,
            active: 0,
            autoHeight: true
        });

        $( ".accordion").not(':eq(0)').accordion({
            collapsible: true,
            active: false,
            autoHeight: true
        });
    }


});


