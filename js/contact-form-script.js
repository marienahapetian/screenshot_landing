$("#contactForm").validator().on("submit", function (event) {
    if (event.isDefaultPrevented()) {
		formError();
    } else {
        event.preventDefault();
        submitForm();
    }
});


function submitForm(){
    // Initiate Variables With Form Content
    var fname = $("#fname").val();
	var lname = $("#lname").val();
    var email = $("#email").val();
    var message = $("#message").val();

    $('.with-errors').html('');

    $.ajax({
        type: "POST",
        url: "contact.php",
        data: "fname=" + fname + "&lname=" + lname + "&email=" + email + "&message=" + message,
        dataType: 'json',
        success : function(data){
            if (data.success){
                formSuccess();
            } else {
                formError();
                submitMSG(false, data.errors);
            }
        }
    });
}

function formSuccess(){
    $("#contactForm")[0].reset();
    submitMSG(true, "Thank you! Your email has been sent successfully.")
}

function formError(data){
    $("#contactForm").removeClass().addClass('animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $(this).removeClass();

    });
}
function submitMSG(valid, msgs){
    if(valid){
        var msgClasses = "text-success";

    } else {
        var msgClasses = "text-danger";
    }

    for (var key in msgs) {
        if (msgs.hasOwnProperty(key)) {
            var error = "<ul>";
            if($.isArray(msgs[key])){
                $.each(msgs[key], function( index, value ) {
                    error += '<li>'+value+'</li>';
                });
            } else {
                error += '<li>'+msgs[key]+'</li>';
            }

            error += "</ul>";

            $('#'+key).closest('.form-group').addClass('has-error').find('.with-errors').html(error);
        }
    }

    if(!$.isArray(msgs))  $("#msgSubmit").removeClass().addClass(msgClasses).text(msgs);
}