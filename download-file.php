<?php
require_once __DIR__.'/autoload.php';

ignore_user_abort(true);
set_time_limit(0);

$path = $config['download_file_location'];

$dl_file = preg_replace("([^\w\s\d\-_~,;:\[\]\(\).]|[\.]{2,})", '', 'Screenley.exe');
$dl_file = filter_var($dl_file, FILTER_SANITIZE_URL);
$fullPath = $path . $dl_file;

if ($fd = fopen($fullPath, "r")) {
    if (isWindows()) {
        $db = new DBConnection();
        $db->dbConnect();

        do {
            $token = randomString(20);
        } while (mysqli_num_rows($db->selectWhere('app_downloads', 'token', '=', $token, 'char')));

        $system_info = systemInfo();
        date_default_timezone_set("Asia/Yerevan");
        $result = $db->insertInto(
            'app_downloads',
            [
                'ip_address',
                'city',
                'country',
                'token',
                'device',
                'os',
                'browser',
                'created_at'
            ],
            [
                [
                    get_client_ip(),
                    ip_info(null, 'city'),
                    ip_info(null, 'country'),
                    $token,
                    $system_info['device'],
                    $system_info['os'],
                    get_client_browser(),
                    date('Y-m-d H:i:s')
                ]
            ]
        );

        $db->dbDisconnect();

        $fsize = filesize($fullPath);
        $path_parts = pathinfo($fullPath);
        $ext = strtolower($path_parts["extension"]);
        switch ($ext) {
            case "pdf":
                header("Content-type: application/pdf");
                header("Content-Disposition: attachment; filename=\"" . $path_parts["basename"] . "\""); // use 'attachment' to force a file download
                break;
            // add more headers for other content types here
            default;
                header("Content-type: application/octet-stream");
                header("Content-Disposition: filename=\"" . $path_parts["basename"] . "\"");
                break;
        }
        header("Content-length: $fsize");
        header("Cache-control: private");


        while (!feof($fd)) {
            $buffer = fread($fd, 2048);
            echo $buffer;
        }

        fclose($fd);

        exit;

    }


}

?>
