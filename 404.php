<?php $title = "Page Not Found";?>
<?php include_once __DIR__ . "/inc/header.php"; ?>

<div id="content" class="clearfix">
    <section  class="section-spacing text-center bg-white">
        <h1 style="font-size: 70px;color: #013e7b;">404</h1>
        <h2 style="color: #6d6e71;">Page Not Found</h2>
    </section>
</div>

<?php include_once __DIR__ . "/inc/footer.php"; ?>