<?php

if( $_SERVER['REQUEST_METHOD'] !== 'POST' ){
    http_response_code(404);
    include('404.php');
    die();
}

require_once __DIR__.'/autoload.php';

$errors = array();

if(!isset($_POST['message']) || $_POST['message']==''){
    $errors['message'] = 'Message Is Required';
}

if(!isset($_POST['fname']) || $_POST['fname']==''){
    $errors['fname'] = 'First Name Is Required';
}

if(!isset($_POST['lname']) || $_POST['lname']==''){
    $errors['lname'] = 'Last Name Is Required';
}

if(!isset($_POST['email']) || $_POST['email']==''){
    $errors['email'][] = 'Email Is Required';
} elseif(isset($_POST['email']) && !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
    $errors['email'][] = 'Incorrect Email Format';
}

if(count($errors)) {
    echo json_encode(['success' => false,'error'=> 'Please fix all the errors', 'errors' => $errors]);
    die;
}

date_default_timezone_set('America/Toronto');

$mail             = new PHPMailer();
$mail->IsSMTP();
$mail->SMTPDebug  = 0;                     // enables SMTP debug , 1 = errors and messages , 2 = messages only
$mail->SMTPAuth   = true;                  // enable SMTP authentication
$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
$mail->Host       = "smtp.gmail.com";
$mail->Port       = 465;

//mail subject
$mail->Subject = "Screenley Contact Us";


//mail content
$message = '<html><body><div style=" padding: 15px; background: #f7fcff">';
$message .= '<table rules="all" style="border-color: #5da9dd;" cellpadding="7">';
$message .= "<tr><td><strong>Email:</strong> </td><td>" . strip_tags($_POST['email']) . "</td></tr>";
$message .= "<tr><td style='white-space: nowrap'><strong>Name:</strong> </td><td>" . strip_tags($_POST['fname']) .' '.strip_tags($_POST['lname']). "</td></tr>";

$text = htmlentities($_POST['message']);
if (($text) != '') {
    $message .= "<tr><td><strong>Message:</strong> </td><td>" . $text . "</td></tr>";
}
$message .= "</table>";
$message .= "</div></body></html>";


$mail->MsgHTML($message);

//sender
$mail->Username   = "screenley@gmail.com";
$mail->Password   = "Softcode888";

//send mail to admin from app
$mail->SetFrom($mail->Username, 'Screenley App');

//reply to user
$mail->addReplyTo($_POST['email'], $_POST['fname'].' '.$_POST['lname']);

// admin
$mail->AddAddress("tsaturyan.davit@gmail.com", 'Tsaturyan Davit');

if(!$mail->Send()) {
    echo json_encode(['success' => false,'error'=>$mail->ErrorInfo]);
} else {
    echo json_encode(['success' => true,'error'=>'']);
}

?>