<?php
spl_autoload_register(function($className) {
    include_once 'classes/'.$className . '.php';
});

require_once __DIR__. '/helpers.php';

$GLOBALS['config'] = require_once __DIR__. '/config.php';